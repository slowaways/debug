# PHP Debug - v0.1 Release
Helper for debugging/testing variables with delimiters of call and identifiers of variables.

## Overview

### Delimiters
- \Debug::$from
- \Debug::$to
- \Debug::$search

### Identifiers
- \Debug::$vars
- \Debug::$trace (trace)
- \Debug::$title (call)
- \Debug::$labels ($vars)

## Requirements
- PHP 7.0+
